# This script requires "tea" and "git"
# You also need to have the login setted up
echo "What is the instance's url?"
read URL

REPOS=$(tea repos | awk '{ print $2 }' | sed 1,2d)

for REPO in $REPOS
do
	git clone $URL/$REPO
done
