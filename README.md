# GiteaMove
Move all your Gitea repositories from one instance to another.

> For now, GiteaMove can only backup the GIT repositories. In the future it will backup issues, pulls and will be able to restore them.

This project **will be out of date**. This project is only there for the time [this issue](https://github.com/go-gitea/gitea/issues/8380) and [this issue](https://gitea.com/gitea/tea/issues/22) are not implemented yet.

## Install on Linux
Just copy and paste the following snippet in your terminal

```bash
git clone https://gitea.com/chopin42/GiteaMove && cd GiteaMove
make tea
```

Now you need to connect `tea` with your Gitea account. 

To do so, go into your settings

![settings](./settings1.png)

Then go into `Application`

![settings](./settings2.png)

Then create a new token

![token](./token1.png)

Don't forget to copy and keep the token (at the top of the page)!

![token copy](./token2.png)

Once you done all of that, you can now connect tea with your account by running the following command:

```bash
tea login add --name <your username> --url <the url of your instance> --token <your token>
```

## Backup all your repos
To download all your repositories you can run the following command:

```bash
make download
```

## Todo
- [ ] Being able to download the issues
- [ ] Being able to download the pulls
- [ ] Being able to restore the data to another instance
