import os, json 

# Download all the repos
instance = input("What is the instance's url? ")
# TODO

# Get the list of repos and create the architecture only folders
repos = next(os.walk("."))[1]

# For each repo, parse the issues
tree = {}
for repo in repos:
	# Get the architecture of the tree
	os.chdir(repo)
	tree[repo] = []

	# Get the issues
	issues = os.popen("tea issues --output=csv").read().split("\n")
	del(issues[0])
	del(issues[-1])

	for issue in issues:
		# Split the issues
		issue = issue.split(",")
		
		# Find the body
		body = os.popen(f"tea issues {issue[0]}").read().split("\n")
		body = body[3:]
		body = ''.join(body)

		# Add the entry to the dictionary
		tree[repo].append({
			"index": issue[0],
			"state": issue[1],
			"author": issue[2],
			"updated": issue[3],
			"title": issue[4],
			"body": body
		})


# Print the full thing
print(json.dumps(tree, indent=4))