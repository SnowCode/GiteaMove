ISSUES=$(tea issues --output=simple | awk '{ print $1 }')

for ISSUE in $ISSUES
do
	CONTENT=$(tea issues $ISSUE | sed 1,3d)
	ISSUES=$(echo -e "$ISSUES" | sed '$ISSUEs/$/ $CONTENT/')
done

echo -e "$ISSUES"


